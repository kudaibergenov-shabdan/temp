import React from 'react';
import './DishInCart.css';
import {useDispatch, useSelector} from "react-redux";
import {dropDish} from "../../store/actions/shoppingCartAction";

const DishInCart = props => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.shoppingCartReducer.dishes);

    const dropDishClicked = dishName => {
        dispatch(dropDish(dishName));
    };

    return (
        <>
            {(dishes[props.dishName].count > 0)
                && <div className="DishInCart">
                    <div className="info-box">
                        <div className="info-box__title">
                            {props.dishName}&nbsp;
                            x{props.dishProp.count}
                        </div>
                        <div className="info-box__cost">
                            {props.dishProp.cost}
                        </div>
                        <div className="info-box__amount">
                            {props.dishProp.amount}
                        </div>
                        <div className="info-box__drop">
                            <button onClick={() => dropDishClicked(props.dishName)}>drop</button>
                        </div>
                    </div>
                </div>
            }
        </>
    );
};

export default DishInCart;