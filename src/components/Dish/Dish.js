import React, {useEffect} from 'react';
import './Dish.css';
import {useDispatch} from "react-redux";
import {addDish, initShoppingCart} from "../../store/actions/shoppingCartAction";

const Dish = ({product}) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(initShoppingCart(product));
    }, [dispatch, product]);

    const addToShoppingCart = e => {
        const dishName = e.target.name;
        dispatch(addDish(dishName));
    }

    return (
        <div className="Dish">
            <div className="block-img">
                <img src={product.image} alt={product.name}/>
            </div>
            <div className="block-info">
                <p className="title">{product.name}</p>
                <p className="title">USD {product.cost}</p>
            </div>
            <div className="block-btn">
                <button className="btn-cart"
                        name={product.name}
                        onClick={addToShoppingCart}
                >
                    Add to cart
                </button>
            </div>
        </div>
    );
};

export default Dish;