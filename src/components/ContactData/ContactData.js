import React, {useState} from 'react';
import './ContactData.css';
import Button from "../UI/Button/Button";
import {useDispatch} from "react-redux";
import {makeOrder} from "../../store/actions/shoppingCartAction";


const ContactData = () => {
    const dispatch = useDispatch();

    const [customer, setCustomer] = useState({
        name: '',
        phone: '',
        street: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setCustomer(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const createOrder = e => {
        e.preventDefault();
        dispatch(makeOrder(customer));
    };

    let form = (
        <form onSubmit={createOrder}>
            <input
                className="Input"
                type="text"
                name="name"
                placeholder="Your Name"
                value={customer.name}
                onChange={onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="phone"
                placeholder="Phone number"
                value={customer.email}
                onChange={onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="street"
                placeholder="Street"
                value={customer.street}
                onChange={onInputChange}
            />
            <Button type="Success">ORDER</Button>
        </form>
    );

    return (
        <div className="ContactData">
            <h4>Enter your Contact Data</h4>
            {form}
        </div>
    );
};

export default ContactData;