import React from 'react';
import './Cart.css';
import {useDispatch, useSelector} from "react-redux";
import DishInCart from "../DishInCart/DishInCart";
import {setModalOpen} from "../../store/actions/shoppingCartAction";

const Cart = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.shoppingCartReducer.dishes);
    const counter = useSelector(state => state.shoppingCartReducer.counter);

    const placeOrderClick = () => {
        dispatch(setModalOpen(true));
    }
    return (
        <div className="Cart">
            <p className="cart-title">Cart</p>
            {

                (counter > 0)
                    ?
                    (
                        <>
                            {Object.keys(dishes).map(dish => (
                                <DishInCart key={dish} dishName={dish} dishProp={dishes[dish]}/>
                            ))}
                            <div style={{textAlign: "center"}}>
                                <button onClick={placeOrderClick}>Place Order</button>
                            </div>
                        </>
                    )
                    : <p>No products into Shopping cart</p>
            }

        </div>
    );
};

export default Cart;