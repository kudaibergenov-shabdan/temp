import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productAction";
import Dish from "../Dish/Dish";
import './Menu.css';

const Menu = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.productReducer.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <div className="Menu">
            <p className="menu-title">Menu</p>
            {products && (Object.keys(products).map(key => (
                <Dish key={key}
                      product={products[key]}
                />
            )))}
        </div>
    );
};

export default Menu;