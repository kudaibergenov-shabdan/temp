import Layout from "./components/UI/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import Order from "./containers/Order/Order";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Order} />
            <Route render={() => <h1>Not found</h1>} />
        </Switch>
    </Layout>
);

export default App;
