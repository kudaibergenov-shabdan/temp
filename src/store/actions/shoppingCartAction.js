import axiosApi from "../../axiosApi";

export const ADD_DISH = 'ADD_DISH';
export const INIT_SHOPPING_CART = 'INIT_SHOPPING_CART';
export const DROP_DISH = 'DROP_DISH';
export const OPEN_MODAL = 'OPEN_MODAL';
export const MAKE_ORDER = 'MAKE_ORDER';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILED = 'ORDER_FAILED';

export const initShoppingCart = product => {
    return {type: INIT_SHOPPING_CART, payload: product}
};

export const addDish = dish => {
    return {type: ADD_DISH, payload: dish};
};

export const dropDish = dish => {
    return {type: DROP_DISH, payload: dish};
};

export const setModalOpen = isOpen => {
    return {type: OPEN_MODAL, payload: isOpen};
};

const makeOrderSuccess = () => {
    return {type: ORDER_SUCCESS};
};

const makeOrderFail = () => {
    return {type: ORDER_FAILED}
}

export const makeOrder = customer => {
    return async (dispatch, getState) => {
        const dishes = getState().shoppingCartReducer.dishes;
        const orderedDishes =
            Object.keys(dishes)
                .map(dish => {
                    return {name: dish, count: dishes[dish].count};
                })
                .filter(dish => {
                    return dish.count > 0
                });
        try {
            await axiosApi.post('/orders.json', {
                customer,
                orderedDishes
            });
            dispatch(makeOrderSuccess());
        } catch (error) {
            dispatch(makeOrderFail());
        }
    }
}