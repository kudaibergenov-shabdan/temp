import axiosApi from "../../axiosApi";

export const INIT_PRODUCTS_FAILURE = 'INIT_PRODUCTS_FAILURE';
export const INIT_PRODUCTS_SUCCESS = 'INIT_PRODUCTS_SUCCESS';

const productErrorHandler = () => ({type: INIT_PRODUCTS_FAILURE});
const initProducts = products => ({type: INIT_PRODUCTS_SUCCESS, payload: products});

export const fetchProducts = () => {
    return async dispatch => {
        try {
            const products = await axiosApi.get('/products.json');
            dispatch(initProducts(products.data));
        } catch (error) {
            dispatch(productErrorHandler());
        }
    }
}
