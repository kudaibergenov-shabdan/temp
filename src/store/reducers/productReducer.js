import {INIT_PRODUCTS_FAILURE, INIT_PRODUCTS_SUCCESS} from "../actions/productAction";

const initialState = {
    products: {}
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_PRODUCTS_FAILURE:
            return state;
        case INIT_PRODUCTS_SUCCESS:
            return {...state, products: action.payload};
        default:
            return state;
    }
};

export default productReducer;