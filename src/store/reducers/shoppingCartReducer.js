import {
    ADD_DISH,
    DROP_DISH,
    INIT_SHOPPING_CART,
    OPEN_MODAL,
    ORDER_FAILED,
    ORDER_SUCCESS
} from "../actions/shoppingCartAction";

const initialState = {
    counter: 0,
    isModalOpen: false,
    dishes: {}
}

const shoppingCartReducer = (state = initialState, action) => {
    let dishName = undefined;
    let dishCount = undefined;
    switch (action.type) {
        case INIT_SHOPPING_CART:
            dishName = action.payload.name;
            const dishProperties = {cost: action.payload.cost, count: 0, amount: 0};
            return {
                ...state,
                dishes: {
                    ...state.dishes, [dishName]: dishProperties
                }
            };
        case ADD_DISH:
            dishName = action.payload;
            dishCount = state.dishes[dishName].count + 1;
            return {
                ...state,
                counter: state.counter + 1,
                dishes: {
                    ...state.dishes,
                    [dishName]: {
                        ...state.dishes[dishName],
                        count: dishCount,
                        amount: dishCount * state.dishes[dishName].cost
                    }
                }
            };
        case DROP_DISH:
            dishName = action.payload;
            return {
                ...state,
                counter: state.counter - state.dishes[dishName].count,
                dishes: {
                    ...state.dishes,
                    [dishName]: {
                        ...state.dishes[dishName],
                        count: 0,
                        amount: 0
                    }
                }
            };
        case OPEN_MODAL:
            return {
                ...state,
                isModalOpen: action.payload
            };
        case ORDER_SUCCESS:
            return {
                ...state,
                isModalOpen: false,
                dishes: {},
                counter: 0
            };
        case ORDER_FAILED:
            return state;
        default:
            return state;
    }
};

export default shoppingCartReducer;