import React from 'react';
import './Order.css';
import Menu from "../../components/Menu/Menu";
import Cart from "../../components/Cart/Cart";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import {useDispatch, useSelector} from "react-redux";
import {setModalOpen} from "../../store/actions/shoppingCartAction";

const Order = () => {
    const dispatch = useDispatch();
    const isModalOpen = useSelector(state => state.shoppingCartReducer.isModalOpen);

    const closeModal = () => {
        dispatch(setModalOpen());
    };

    return (
        <>
            <Modal
                show={isModalOpen}
                close={closeModal}
            >
                <OrderSummary />
            </Modal>
            <div className="Order">
                <Menu/>
                <Cart/>
            </div>
        </>
    );
};

export default Order;